# Teste SIGA AUTO

Este projeto tem como objetivo avaliar a proeficiência dos candidatos ao estágio na SIGA AUTO.


## O que deverá ser desenvolvido

Deverá ser desenvolvido um sistema de automóveis seguindo o layout apresentado na seção "Layout". O sistema deve ser capaz de realizar um CRUD (Create, Read, Update & Delete) de automóveis utilizando a stack PHP, Laravel e MySQL.

## Iniciando o projeto

1. Instale o [Docker](https://www.docker.com/)
2. Instale o [Git](https://git-scm.com/downloads)
3. No seu terminal, clone o repositório utilizando o comando `git clone https://gitlab.com/edsonlima2506/teste-sigaauto.git`
4. Acesse a pasta do projeto utilizando o comando `cd teste-sigaauto`
5. Crie uma nova branch a partir da master com o comando `git checkout -b nome-da-sua-branch`
6. Abra o projeto utilizando o comando `code .`

## Rodando localmente (Sail/Docker)

Para este projeto será usado o Laravel Sail para o ambiente Docker local, daremos algumas dicas de como utilizar da melhor maneira possível o Laravel Sail:

Instalar dependencias

```bash
  docker run --rm \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install
```

Subir o container docker

```bash
  ./vendor/bin/sail up
```

Comandos artisan

```bash
  ./vendor/bin/sail artisan
```

#### Dica: você pode criar um alias para este comando da seguinte maneira `alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'`, com o alias criado, os comandos ficam assim:

Subir o container docker

```bash
  sail up
```

Comandos artisan

```bash
  sail artisan
```
## Funcionalidades

- Create: deverá ser possível criar um novo automóvel no banco de dados.
- Read: deverá ser possível ler as informações do automóvel.
- Update: deve ser possível atualizar as informações de um automóvel.
- Delete: deve ser possível deletar um automóvel.


## Layout

https://www.figma.com/file/DQJZ5PyLYYc5rIHI2e7nei/Teste-SIGA-AUTO?node-id=0%3A1&t=qv7XfJjky0fT4Nm1-1


## Banco de Dados

![diagrama banco de dados](https://images2.imgbox.com/6d/39/bbqlJ0FN_o.png)

Deve ser criada uma tabela com o nome "cars" que será responsável por armazenar os dados dos carros. Repare que os itens "code" e "license_plate" são únicos, ou seja, só podem pertencer a um carro.

Também esperamos que seja criada uma tabela "marcas", as marcas devem ser inseridas nessa tabela através de um seeder simples. As marcas tem relação one-to-many (um pra muitos) com os carros, ou seja, uma marca pode conter vários carros, essa relação é estabelecida através da foreing key "brand_id" na tabela de carros.

## Entregando o Projeto

Durante o desenvolvimento faça commits do seu desenvolvimento (mas não envie para o GitLab ainda). Assim que finalizar o projeto suba as alteraçõe seguindo o passo a passo:

Commit

```bash
  git add . && git commit -m "Sua mensagem"
```

Push

```bash
  git push -u origin nome-da-sua-branch
```
